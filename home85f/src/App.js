import React from 'react';
import Layout from './components/Layout/Layout';
import { CssBaseline } from '@material-ui/core';
import './App.css';
import { Route, Switch } from 'react-router-dom';
import MainList from './containers/MainList';
import ArtistAlbums from './containers/ArtistAlbums/ArtistAlbums';
import AlbumsTrack from './containers/AlbumsTrack/AlbumsTrack';
import { useSelector } from 'react-redux';
import Login from './containers/User/Login';
import Register from './containers/User/Register';
import TrackHistory from './containers/TrackHistory/TrackHistory';
import AddAlbumsForm from './components/UI/AddAlbumsForm/AddAlbumsForm';
import AddArtistForm from './components/UI/AddArtistForm/AddArtistForm';
import AddTrackForm from './components/UI/AddTrackForm/AddTrackForm';

function App() {
  const user = useSelector(state => state.users.user)
  return (
    <CssBaseline>
      <Layout>
        <Switch>
          <Route exact path="/" render={() => (<MainList user={user} />)} />
          <Route exact path="/artists/:id" component={ArtistAlbums} />
          <Route exact path="/albums/:id/:name" component={AlbumsTrack} />
          <Route exact path="/artist/add-artist" component={AddArtistForm} />
          <Route exact path="/album/add-album" component={AddAlbumsForm} />
          <Route exact path="/track/add-track" component={AddTrackForm} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/track_history" component={TrackHistory} />
          <Route render={() => (<div>not found 404</div>)} />
        </Switch>
      </Layout>
    </CssBaseline>
  );
}

export default App;
