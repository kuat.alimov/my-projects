const port = process.env.REACT_APP_NODE_ENV !== "test" ? 8000 : 8010;
export const apiURL = 'http://localhost:' + port;
