import {
    GET_TRACKS,
    TRACKS_REQUEST,
    TRACKS_SUCCESS,
    TRACKS_ERROR,
} from './trackActionType';
import axios from '../../axios-api';
import { push } from 'connected-react-router';

export const getTracks = (value) => {
    return { type: GET_TRACKS, value }
};

export const tracksRequest = () => ({ type: TRACKS_REQUEST });
export const tracksSuccess = () => ({ type: TRACKS_SUCCESS });
export const tracksError = (error) => ({ type: TRACKS_ERROR, error });

export const getResponseTracks = (album_id, { token }) => {
    return async dispatch => {
        try {
            let tracksUrl = '/tracks'
            if (album_id !== null) {
                tracksUrl = `/tracks?album=${album_id}`
            }
            dispatch(tracksRequest());
            const response = await axios.get(tracksUrl, { headers: { "Authorization": `${token}` } });
            if (response.data !== null) {
                dispatch(getTracks(response.data));
                dispatch(tracksSuccess());
            }

        } catch (err) {
            dispatch(tracksError(err));
        }
    }
};

export const addNewTracks = (dataCopy, { token }) => {
    return async dispatch => {
        try {
            let tracksUrl = '/tracks';
            dispatch(tracksRequest());
            await axios.post(tracksUrl, dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(tracksSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(tracksError(e.response.data));
            } else {
                dispatch(tracksError({ global: 'No internet' }));
            }
        }
    }
};

export const publishTrack = (id, dataCopy, { token }) => {
    return async dispatch => {
        try {
            dispatch(tracksRequest());
            await axios.post(`/tracks/${id}/publish`, dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(tracksSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(tracksError(e.response.data));
            } else {
                dispatch(tracksError({ global: 'No internet' }));
            }
        }
    }
};


export const deleteTrack = (id, { token }) => {
    return async dispatch => {
        try {
            dispatch(tracksRequest());
            await axios.delete(`/tracks/${id}`, { headers: { "Authorization": `${token}` } });
            dispatch(tracksSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(tracksError(e.response.data));
            } else {
                dispatch(tracksError({ global: 'No internet' }));
            }
        }
    }
};
