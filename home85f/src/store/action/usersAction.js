import axios from '../../axios-api';
import { push } from 'connected-react-router';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS';

export const registerUserSuccess = () => {
    return { type: REGISTER_USER_SUCCESS }
}

export const registerUserFailure = (err) => {
    return { type: REGISTER_USER_FAILURE, err }
}

export const registerUser = (userData) => {
    return async dispatch => {
        try {
            await axios.post("/users", userData)
            dispatch(registerUserSuccess())
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(registerUserFailure(e.response.data));
            } else {
                dispatch(registerUserFailure({ global: 'No internet' }));
            }
        }

    }
}

export const logoutUserSuccess = () => {
    return { type: LOGOUT_USER_SUCCESS }
}

export const logoutUser = () => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        await axios.delete("/users/sessions", { headers: { "Authorization": `${token}` } });
        dispatch(logoutUserSuccess())
        dispatch(push('/'));
    }
}

export const loginUserSuccess = (user) => {
    return { type: LOGIN_USER_SUCCESS, user }
}

export const loginUserFailure = (err) => {
    return { type: LOGIN_USER_FAILURE, err }
}

export const loginUser = (userData) => {
    return async dispatch => {
        try {
            const response = await axios.post("/users/sessions", userData)

            dispatch(loginUserSuccess(response.data))
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(loginUserFailure(e.response.data));
            } else {
                dispatch(loginUserFailure({ global: 'No internet' }));
            }
        }

    }
};

export const facebookLogin = (dataCopy) => {
    return async dispatch => {
        try {
            const response = await axios.post('/users/facebooklogin', dataCopy);

            dispatch(loginUserSuccess(response.data.user));
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(loginUserFailure(e.response.data));
            } else {
                dispatch(loginUserFailure({ global: 'No internet' }));
            }
        }
    }
};