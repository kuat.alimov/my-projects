import axios from '../../axios-api';

export const TRACKHISTORY_VALUE = 'TRACKHISTORY_VALUE';
export const TRACKHISTORY_REGISTRATION_SUCCESS = 'TRACKHISTORY_REGISTRATION_SUCCESS';
export const TRACKHISTORY_REGISTRATION_FAILURE = 'TRACKHISTORY_REGISTRATION_FAILURE';

export const registerTrackHistorySuccess = () => {
    return { type: TRACKHISTORY_REGISTRATION_SUCCESS }
}

export const registerTrackHistoryFailure = (err) => {
    return { type: TRACKHISTORY_REGISTRATION_FAILURE, err }
}

export const getTrackHistory = (value) => {
    return { type: TRACKHISTORY_VALUE, value }
}

export const getTrackHistoryData = ({ token }) => {
    return async dispatch => {
        try {
            const response = await axios.get("/track_history", { headers: { "Authorization": `${token}` } })

            dispatch(getTrackHistory(response.data));
            dispatch(registerTrackHistorySuccess());
            // dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(registerTrackHistoryFailure(e.response.data));
            } else {
                dispatch(registerTrackHistoryFailure({ global: 'No internet' }));
            }
        }

    }
}

export const registerTrack = (track_id, { token }) => {
    return async dispatch => {
        try {
            await axios.post("/track_history", track_id, { headers: { "Authorization": `${token}` } })

            dispatch(registerTrackHistorySuccess())
            // dispatch(push('/'));
        } catch (e) {
            dispatch(registerTrackHistoryFailure(e));
        }

    }
}