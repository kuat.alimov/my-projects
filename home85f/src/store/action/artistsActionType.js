export const GET_ARTISTS = 'GET_ARTISTS';
export const ARTISTS_REQUEST = 'ARTISTS_REQUEST';
export const ARTISTS_SUCCESS = 'ARTISTS_SUCCESS';
export const ARTISTS_ERROR = 'ARTISTS_ERROR';
