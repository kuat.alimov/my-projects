import {
    GET_ARTISTS,
    ARTISTS_REQUEST,
    ARTISTS_SUCCESS,
    ARTISTS_ERROR,
} from './artistsActionType';
import axios from '../../axios-api';
import { push } from 'connected-react-router';

export const getArtists = (value) => {
    return { type: GET_ARTISTS, value }
};

export const artistsRequest = () => ({ type: ARTISTS_REQUEST });
export const artistsSuccess = () => ({ type: ARTISTS_SUCCESS });
export const artistsError = (error) => ({ type: ARTISTS_ERROR, error });

export const getResponseArtists = () => {
    return async dispatch => {
        try {
            dispatch(artistsRequest());
            const response = await axios.get('/artists');
            if (response.data !== null) {

                const artistsCopy = response.data.map(element => {
                    if (element.image && element.image === '') {
                        return {
                            ...element,
                            image: null,
                        }
                    } else {
                        return {
                            ...element,
                        }
                    }
                });
                dispatch(getArtists(artistsCopy));
                dispatch(artistsSuccess());
            }

        } catch (err) {
            dispatch(artistsError(err));
        }
    }
};

export const addNewArtists = (dataCopy, { token }) => {
    return async dispatch => {
        try {
            dispatch(artistsRequest());
            await axios.post('/artists', dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(artistsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(artistsError(e.response.data));
            } else {
                dispatch(artistsError({ global: 'No internet' }));
            }
        }
    }
};

export const publishArtist = (id, dataCopy, { token }) => {
    return async dispatch => {
        try {
            dispatch(artistsRequest());
            await axios.post(`/artists/${id}/publish`, dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(artistsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(artistsError(e.response.data));
            } else {
                dispatch(artistsError({ global: 'No internet' }));
            }
        }
    }
};


export const deleteArtist = (id, { token }) => {
    return async dispatch => {
        try {
            dispatch(artistsRequest());
            await axios.delete(`/artists/${id}`, { headers: { "Authorization": `${token}` } });
            dispatch(artistsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(artistsError(e.response.data));
            } else {
                dispatch(artistsError({ global: 'No internet' }));
            }
        }
    }
};