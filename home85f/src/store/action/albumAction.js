import {
    GET_ALBUMS,
    ALBUMS_REQUEST,
    ALBUMS_SUCCESS,
    ALBUMS_ERROR,
} from './albumActionType';
import axios from '../../axios-api';
import { push } from 'connected-react-router';

export const getAlbums = (value) => {
    return { type: GET_ALBUMS, value }
};

export const albumsRequest = () => ({ type: ALBUMS_REQUEST });
export const albumsSuccess = () => ({ type: ALBUMS_SUCCESS });
export const albumsError = (error) => ({ type: ALBUMS_ERROR, error });

export const getResponseAlbums = (artist_id, { token }) => {
    return async dispatch => {
        try {
            let albumsUrl = '/albums'
            if (artist_id !== null) {
                albumsUrl = `/albums?artist=${artist_id}`
            }
            dispatch(albumsRequest());
            const response = await axios.get(albumsUrl, { headers: { "Authorization": `${token}` } });
            if (response.data !== null) {
                const dataCopy = response.data.map(element => {
                    if (element.image && element.image === '') {
                        return {
                            ...element,
                            datetime: element.datetime.slice(0, -5).split('T').join(' '),
                            image: null,
                        }
                    } else {
                        return {
                            ...element,
                            datetime: element.datetime.slice(0, -5).split('T').join(' ')
                        }
                    }
                });
                dispatch(getAlbums(dataCopy));
                dispatch(albumsSuccess());
            }

        } catch (err) {
            dispatch(albumsError(err));
        }
    }
};

export const addNewAlbums = (dataCopy, { token }) => {
    return async dispatch => {
        try {
            dispatch(albumsRequest());
            await axios.post('/albums', dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(albumsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(albumsError(e.response.data));
            } else {
                dispatch(albumsError({ global: 'No internet' }));
            }
        }
    }
};

export const publishAlbums = (id, dataCopy, { token }) => {
    return async dispatch => {
        try {
            dispatch(albumsRequest());
            await axios.post(`/albums/${id}/publish`, dataCopy, { headers: { "Authorization": `${token}` } });
            dispatch(albumsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(albumsError(e.response.data));
            } else {
                dispatch(albumsError({ global: 'No internet' }));
            }
        }
    }
};

export const deleteAlbum = (id, { token }) => {
    return async dispatch => {
        try {
            dispatch(albumsRequest());
            await axios.delete(`/albums/${id}`, { headers: { "Authorization": `${token}` } });
            dispatch(albumsSuccess());
            dispatch(push('/'));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(albumsError(e.response.data));
            } else {
                dispatch(albumsError({ global: 'No internet' }));
            }
        }
    }
};