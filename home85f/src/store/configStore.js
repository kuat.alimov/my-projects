import { createStore, applyMiddleware, compose, combineReducers } from 'redux';

import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

import artistReducer from './reducer/artist';
import albumsReducer from './reducer/albums';
import tracksReducer from './reducer/track';
import usersReducer from './reducer/user';
import trackHistoryReducer from './reducer/trackHistory';

export const history = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    artists: artistReducer,
    albums: albumsReducer,
    tracks: tracksReducer,
    users: usersReducer,
    tracksHistory: trackHistoryReducer,
    router: connectRouter(history),
});

const middleWare = [
    thunk,
    routerMiddleware(history),
];

const saveToLocalStorage = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    } catch (e) {
        console.log('Error save state');
    }
}

const loadLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);
    } catch (e) {
        return undefined;
    }
}

const enhancers = composeEnhancers(applyMiddleware(...middleWare));
const persistedState = loadLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            user: store.getState().users.user,
        }
    })
})

export default store;