import {
    TRACKHISTORY_VALUE,
    TRACKHISTORY_REGISTRATION_SUCCESS,
    TRACKHISTORY_REGISTRATION_FAILURE,
} from "../action/trackHistoryAction";

const initState = {
    trackHistoryList: [],
    error: null,
}

const trackHistoryReducer = (state = initState, action) => {
    switch (action.type) {
        case TRACKHISTORY_VALUE:
            return { ...state, trackHistoryList: action.value };
        case TRACKHISTORY_REGISTRATION_FAILURE:
            return { ...state, error: action.err };
        case TRACKHISTORY_REGISTRATION_SUCCESS:
            return { ...state, error: null };
        default:
            return state;
    }
}

export default trackHistoryReducer;