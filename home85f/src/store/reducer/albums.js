import {
    GET_ALBUMS,
    ALBUMS_REQUEST,
    ALBUMS_SUCCESS,
    ALBUMS_ERROR,
} from '../action/albumActionType';

const initialState = {
    albumsList: [],
    loading: false,
    error: null,
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ALBUMS_REQUEST:
            return { ...state, loading: true };
        case ALBUMS_SUCCESS:
            return { ...state, loading: false, error: null };
        case ALBUMS_ERROR:
            return { ...state, loading: false, error: action.error };
        case GET_ALBUMS:
            return { ...state, albumsList: action.value };
        default:
            return state;
    };
};

export default albumsReducer;