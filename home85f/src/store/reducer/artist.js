import {
    GET_ARTISTS,
    ARTISTS_REQUEST,
    ARTISTS_SUCCESS,
    ARTISTS_ERROR,
} from '../action/artistsActionType';

const initialState = {
    artistsList: [],
    loading: false,
    error: null,
};

const artistReducer = (state = initialState, action) => {
    switch (action.type) {
        case ARTISTS_REQUEST:
            return { ...state, loading: true };
        case ARTISTS_SUCCESS:
            return { ...state, loading: false, error: null };
        case ARTISTS_ERROR:
            return { ...state, loading: false, error: action.error };
        case GET_ARTISTS:
            return { ...state, artistsList: action.value };
        default:
            return state;
    };
};

export default artistReducer;