import {
    GET_TRACKS,
    TRACKS_REQUEST,
    TRACKS_SUCCESS,
    TRACKS_ERROR,
} from '../action/trackActionType';

const initialState = {
    tracksList: [],
    loading: false,
    error: null,
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case TRACKS_REQUEST:
            return { ...state, loading: true };
        case TRACKS_SUCCESS:
            return { ...state, loading: false, error: null };
        case TRACKS_ERROR:
            return { ...state, loading: false, error: action.error };
        case GET_TRACKS:
            return { ...state, tracksList: action.value };
        default:
            return state;
    };
};

export default tracksReducer;