import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Grid, Button, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import {
    getResponseArtists, publishArtist, deleteArtist
} from '../store/action/artistsAction';
import ArtistItem from '../components/ArtistItem/ArtistItem';
import AddMenu from './AddMenu/AddMenu';


const useStyles = makeStyles(theme => (
    {
        link: {
            margin: theme.spacing(1, 1.5),
            fontFamily: 'Arial, sans-serif',
        },
    }
));

const MainList = ({ user }) => {
    const classes = useStyles();

    const dispatch = useDispatch();
    const history = useHistory();
    const [displayName, setUserName] = useState('');
    const { artistsList } = useSelector(state => state.artists, shallowEqual());

    const allAlbumArtist = (id) => {
        if (user) {
            history.push(`/artists/${id}`);
        } else {
            history.push(`/login`);
        }
    }

    const publishArtistHandler = (id) => {
        dispatch(publishArtist(id, { publicArtist: true }, { ...user }));
        dispatch(getResponseArtists());
    }

    const deleteArtistHandler = (id) => {
        dispatch(deleteArtist(id, { ...user }));
        dispatch(getResponseArtists());
    }

    useEffect(() => {
        dispatch(getResponseArtists());
        if (user) {
            setUserName(user.displayName);
        }

    }, [dispatch]);

    // useMemo(() => {
    //     if (artistsList) {
    //         dispatch(getResponseArtists());
    //     }
    // }, [artistsList])

    let artistsItems;
    if (artistsList) {

        artistsItems = (
            artistsList.map((item, i) => {
                return (
                    <ArtistItem
                        key={i}
                        id={i}
                        imgSrc={item.image}
                        text={item.name}
                        displayName={displayName}
                        description={item.description}
                        publicArtist={item.public}
                        delete={() => deleteArtistHandler(item._id)}
                        publish={() => publishArtistHandler(item._id)}
                        onClick={() => allAlbumArtist(item._id)}
                    />
                )
            })
        )
    }
    return (
        <Grid container direction="column" spacing={2}>
            <AddMenu />
            {artistsItems}
        </Grid>
    )
}

export default MainList;