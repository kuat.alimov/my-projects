import React, { useMemo, useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import Tracks from '../../components/Tracks/Tracks';
import { getTrackHistoryData } from '../../store/action/trackHistoryAction';
import './TrackHistory.css';

const TrackHistory = (props) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user)

    const [tracks, setTracks] = useState([]);
    const { trackHistoryList } = useSelector(state => state.tracksHistory, shallowEqual());

    useEffect(() => {
        if (user) {
            dispatch(getTrackHistoryData({ ...user }));
        } else {
            props.history.push(`/login`);
        }
    }, [dispatch]);

    useMemo(() => {
        if (trackHistoryList) {
            setTracks(trackHistoryList);
        }
    }, [trackHistoryList])


    let tracksHistory;

    if (tracks && tracks.length > 0) {
        tracksHistory = (
            <>
                <div className='TracksHistory-list'>
                    {tracks.map((element, i) => {
                        return (
                            <Tracks
                                key={i}
                                id={i}
                                trackName={element.track}
                                userName={element.username}
                                datetime={element.datetime}
                                onClick={null}
                                publicTrack={true}
                            />
                        )
                    })
                    }

                </div>
            </>
        )
    }

    return (
        <>
            {tracksHistory}
        </>
    );
};

export default TrackHistory;