import React, { useMemo, useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import {
    getResponseAlbums, publishAlbums, deleteAlbum
} from '../../store/action/albumAction';
import './ArtistAlbums.css';
import Albums from '../../components/Albums/Albums';

const ArtistAlbums = (props) => {
    const dispatch = useDispatch();
    const [albums, setAlbums] = useState([]);
    const [displayName, setUserName] = useState('');
    const user = useSelector(state => state.users.user)
    const { albumsList } = useSelector(state => state.albums, shallowEqual());

    const albumShowTracks = (id, name) => {
        if (user) {
            props.history.push(`/albums/${id}/${name}`);
        } else {
            props.history.push(`/login`);
        }
    }

    const publishAlbumHandler = (id) => {
        dispatch(publishAlbums(id, { publicAlbum: true }, { ...user }))
    }

    const deleteAlbumHandler = (id) => {
        dispatch(deleteAlbum(id, { ...user }))
    }

    useEffect(() => {
        const artist_id = props.match.params.id;
        dispatch(getResponseAlbums(artist_id, { ...user }));
        if (user) {
            setUserName(user.displayName);
        }
    }, [dispatch]);

    useMemo(() => {
        if (albumsList) {
            setAlbums(albumsList);
        }
    }, [albumsList])


    let allAlbums;

    if (albums && albums.length > 0) {
        allAlbums = (
            <>
                <div className='ArtistAlbums-list'>
                    <h4>Artist name: {albums[0].artist.name}</h4>
                    {albums.map((element, i) => {
                        return (
                            <Albums
                                key={i}
                                id={i}
                                displayName={displayName}
                                image={element.image}
                                album={element.album}
                                datetime={element.datetime}
                                count={element.count}
                                publicAlbum={element.public}
                                deleteAlbum={() => deleteAlbumHandler(element._id)}
                                publish={() => publishAlbumHandler(element._id)}
                                onClick={() => albumShowTracks(element._id, element.artist.name)}
                            />
                        )
                    })
                    }

                </div>
            </>
        )
    }

    return (
        <div className='Wrapper-ArtistAlbumslist'>
            {allAlbums}
        </div>
    );
};

export default ArtistAlbums;