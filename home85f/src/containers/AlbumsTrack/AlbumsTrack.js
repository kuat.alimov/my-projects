import React, { useMemo, useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import Tracks from '../../components/Tracks/Tracks';
import {
    getResponseTracks, publishTrack, deleteTrack
} from '../../store/action/trackAction';
import { registerTrack } from '../../store/action/trackHistoryAction';
import './AlbumsTrack.css';

const AlbumsTrack = (props) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user)
    const [userName, setUserName] = useState('');

    const [tracks, setTracks] = useState([]);
    const { tracksList } = useSelector(state => state.tracks, shallowEqual());

    const trackHistoryHandler = (id) => {
        dispatch(registerTrack({ track: id }, { ...user }))
    }

    const publishTrackHandler = (id) => {
        dispatch(publishTrack(id, { publicTrack: true }, { ...user }))
    }

    const deleteTrackHandler = (id) => {
        dispatch(deleteTrack(id, { ...user }))
    }

    useEffect(() => {
        const albums_id = props.match.params.id;
        dispatch(getResponseTracks(albums_id, { ...user }));
        if (user) {
            setUserName(user.displayName);
        }
    }, [dispatch]);

    useMemo(() => {

        if (tracksList) {
            setTracks(tracksList);
        }
    }, [tracksList])


    let allTracks;

    if (tracks && tracks.length > 0) {
        allTracks = (
            <>
                <div className='ArtistTracks-list'>
                    <h4>Artist name: {props.match.params.name}</h4>
                    {tracks.map((element, i) => {
                        return (
                            <Tracks
                                key={i}
                                id={i}
                                userNameLogin={userName}
                                track={element.track}
                                trackNumber={element.trackNumber}
                                time={element.time}
                                publicTrack={element.public}
                                deleteTrack={() => deleteTrackHandler(element._id)}
                                publish={() => publishTrackHandler(element._id)}
                                onClick={() => trackHistoryHandler(element._id)}
                            />
                        )
                    })
                    }

                </div>
            </>
        )
    }

    return (
        <>
            {allTracks}
        </>
    );
};

export default AlbumsTrack;