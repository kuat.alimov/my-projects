import React from 'react';
import { useSelector } from 'react-redux';
import { Grid, Button, makeStyles } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(theme => (
    {
        link: {
            margin: theme.spacing(1, 1.5),
            fontFamily: 'Arial, sans-serif',
        },
    }
));

const AddMenu = (props) => {
    const classes = useStyles();

    const user = useSelector(state => state.users.user)

    return (
        <>
            {user &&
                <Grid container direction="row" spacing={2} justify="center">
                    <Grid item>
                        <Button to="/artist/add-artist" color='primary' id='add-artist'
                            variant="outlined" className={classes.link} component={NavLink}>
                            Add artist
                    </Button>
                        <Button to="/album/add-album" color='primary' id='add-album'
                            variant="outlined" className={classes.link} component={NavLink}>
                            Add album
                    </Button>
                        <Button to="/track/add-track" color='primary' id='add-track'
                            variant="outlined" className={classes.link} component={NavLink}>
                            Add track
                    </Button>
                    </Grid>
                </Grid>}
        </>
    );
};

export default AddMenu;