import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Alert from '@material-ui/lab/Alert';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FormElement from '../../components/UI/Form/FormElement';
import {
    loginUser
} from '../../store/action/usersAction';
import FacebookLoginButton from '../../components/FacebookLoginButton/FacebookLoginButton';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alertW: {
        marginTop: theme.spacing(3),
        width: '100%',
    }
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [state, setState] = useState({
        username: '',
        password: '',
    })
    const errors = useSelector(state => state.users.loginError)
    const inputHandlerChange = (e) => {
        const { name, value } = e.target;
        setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            }
        })
    }

    const submitFormHandler = async (event) => {
        event.preventDefault();
        await dispatch(loginUser({ ...state }));
    }

    const getFieldError = (fieldName) => {
        try {
            return errors.errors[fieldName].message;
        } catch {
            return undefined;
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign In
                </Typography>
                {errors && <Alert
                    className={classes.alertW}
                    severity="error"
                >{errors.error}</Alert>}
                <form className={classes.form} noValidate onSubmit={submitFormHandler}>
                    <Grid container spacing={2}>
                        <FormElement
                            label="Enter User name or e-mail"
                            name="username"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('username')}
                        />
                        <FormElement
                            type='password'
                            label="Password"
                            name="password"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('password')}
                        />
                    </Grid>
                    <Button
                        id="signIn"
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                    <FacebookLoginButton />
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link to="/register" variant="body2" component={RouterLink}>
                                Or Sign Up
                    </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

export default Login;