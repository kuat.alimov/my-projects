import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import { Link as RouterLink } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { registerUser } from '../../store/action/usersAction';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FormElement from '../../components/UI/Form/FormElement';
import FileInput from '../../components/UI/Form/FileInput';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));



const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [state, setState] = useState({
        username: '',
        displayName: '',
        email: '',
        password: '',
        avatarImage: '',
    })
    const errors = useSelector(state => state.users.registerError)
    const inputHandlerChange = (e) => {
        const { name, value } = e.target;
        setState((prevState) => {
            return {
                ...prevState,
                [name]: value,
            }
        })
    }

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {
                ...prevState,
                [name]: file,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        await dispatch(registerUser(dataCopy));
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        })
        onFormSubmit(formData);
    }

    const getFieldError = (fieldName) => {
        try {
            return errors.errors[fieldName].message;
        } catch {
            return undefined;
        }
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Register user
                </Typography>
                <form className={classes.form} noValidate onSubmit={submitFormHandler}>
                    <Grid container spacing={2}>
                        <FormElement
                            label="User Name"
                            name="username"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('username')}
                        />
                        <FormElement
                            label="Display Name"
                            name="displayName"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('displayName')}
                        />
                        <FormElement
                            label="Email Address"
                            name="email"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('email')}
                        />
                        <FormElement
                            type='password'
                            label="Password"
                            name="password"
                            onChange={inputHandlerChange}
                            required={true}
                            error={getFieldError('password')}
                        />
                    </Grid>
                    <Grid item>
                        <FileInput
                            label="Avatar Image"
                            name="avatarImage"
                            onChange={fileChangeHandler}
                        />
                    </Grid>
                    <Button
                        id="signUp"
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign Up
          </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link to="/login" variant="body2" component={RouterLink}>
                                Already have an account? Sign in
              </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

export default Register;