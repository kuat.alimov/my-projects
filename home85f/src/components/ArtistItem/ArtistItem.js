import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';

import { apiURL } from '../../apiUrl';
import Button from '@material-ui/core/Button';

import './ArtistItem.css';

const ArtistItem = (props) => {
    const user = useSelector(state => state.users.user)

    let publicArtistCopy = false;
    let enabledBtn = false;
    let publicBtn = false;
    let imgSRC;
    if (!props.publicArtist) {
        if (props.displayName === "admin") {
            publicArtistCopy = true;
            enabledBtn = true;
            publicBtn = false;
        }
    } else {
        if (props.displayName === "admin") {
            enabledBtn = true;
        }
        publicBtn = true;
        publicArtistCopy = true;
    }
    if (!user) {
        enabledBtn = false;
        publicBtn = true;
        if (!props.publicArtist) {
            publicArtistCopy = false;
        }
    }
    if (props.imgSrc) {
        imgSRC = `${apiURL}/uploads/${props.imgSrc}`
    }
    return (
        <>
            {publicArtistCopy ?
                <div className='Wrapper-Executor'>
                    <div name={props.text}
                        className='Wrapper-click' onClick={props.onClick} id={props.text}>
                        <div className='Executor-img'>
                            {props.imgSrc ? <div className="ImgWrapp">
                                <img className='Img-Executor' src={imgSRC} />
                            </div> : <div className="ImgWrapp">
                                    <div className="No-Img"></div>
                                </div>}
                        </div>
                        <div className='Executor-wrapp'>
                            <p className='Executor-text'><b>Name executor:</b> {props.text}</p>
                            <p className='Executor-text'><b>description:</b> {props.description}</p>
                        </div>
                    </div>
                    <div className='Executor-publish'>
                        {publicBtn ? <p className='Executor-text public-artist'>опубликовано </p> :
                            <>
                                <p className='Executor-text unpublic-artist'>Не опубликовано </p>
                                <Button
                                    variant="contained"
                                    onClick={props.publish}
                                >Опубликовать</Button>
                            </>
                        }
                        {enabledBtn ? <>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={props.delete}
                            >Delete</Button>
                        </> : null
                        }
                    </div>
                </div>
                : null}
        </>
    )
}

export default ArtistItem;