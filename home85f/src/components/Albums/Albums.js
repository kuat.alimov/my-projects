import React from 'react';
import { useSelector } from 'react-redux';

import './Albums.css';
import { apiURL } from '../../apiUrl';
import Button from '@material-ui/core/Button';

const Albums = ({ publicAlbum, id, image, album, datetime, count, onClick, publish, displayName, deleteAlbum }) => {
    const user = useSelector(state => state.users.user)

    let publicAlbumCopy = false;
    let enabledBtn = false;

    if (!publicAlbum) {
        if (displayName === "admin") {
            publicAlbumCopy = true;
            enabledBtn = true;
        }
    } else {
        if (displayName === "admin") {
            enabledBtn = true;
        }
        publicAlbumCopy = true;
    }
    if (!user) {
        enabledBtn = false;
        publicAlbumCopy = false;
    }
    return (
        <>
            {publicAlbumCopy ?
                <div key={id}
                    className='ArtistAlbums'>
                    <div className='Block-cursor'
                        onClick={onClick} id={album} name={album}
                    >
                        <div className='ArtistAlbums-img'
                            style={{ marginRight: 'auto' }}>
                            {image ? <div className="ImgWrappArtistAlbums">
                                <img className='Img-ArtistAlbums' src={`${apiURL}/uploads/${image}`} />
                            </div> : <div className="ImgWrappArtistAlbums">
                                    <div className="No-Img"></div>
                                </div>}
                        </div>
                        <div className='ArtistAlbums-info'>
                            <p className='Info--param'><b>Album name:</b> {album}</p>
                            <p className='Info--param'><b>Release year:</b> {datetime}</p>
                            <p className='Info--param'><b>Tracks count:</b> {count}</p>
                        </div>
                    </div>
                    <div className='ArtistAlbums-publish'>

                        {publicAlbum ? <p className='Info--param public-album'>опубликовано</p> :
                            <>
                                <p className='Info--param unpublic-album'>Не опубликовано</p>
                                <Button
                                    fullWidth
                                    variant="contained"
                                    onClick={publish}
                                >Опубликовать</Button>
                            </>
                        }
                        {enabledBtn ? <>
                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={deleteAlbum}
                            >Delete</Button>
                        </> : null
                        }
                    </div>
                </div>
                : null}
        </>
    );
};

export default Albums;