import React from 'react';
import { useSelector } from 'react-redux';

import './Tracks.css';
import Button from '@material-ui/core/Button';

const Tracks = ({ userNameLogin, id, track, trackNumber, time, onClick, userName, trackName, datetime, publicTrack, publish, deleteTrack }) => {
    const user = useSelector(state => state.users.user)

    let dateTime;
    let publicTrackCopy = false;
    let enabledBtn = false;

    if (!publicTrack) {
        if (userNameLogin === "admin") {
            publicTrackCopy = true;
            enabledBtn = true;
        }
    } else {
        if (userNameLogin === "admin") {
            enabledBtn = true;
        }
        publicTrackCopy = true;
    }
    if (!user) {
        enabledBtn = false;
        publicTrackCopy = false
    }
    if (datetime) {
        dateTime = datetime.slice(0, -5).split('T').join(' ');
    }
    return (
        <>
            {publicTrackCopy ?
                <div key={id}
                    className='Tracks'>
                    <div className='Tracks-click' name={trackNumber}
                        onClick={onClick} id={trackNumber}>
                        {trackNumber ?
                            <p className='Info--param'><b>Track number:</b> {trackNumber}</p> :
                            <p className='Info--param'><b>User name:</b> {userName}</p>}
                        {track ?
                            <p className='Info--param'><b>Track :</b> {track}</p> :
                            <p className='Info--param'><b>Track :</b> {trackName}</p>}
                        {time ?
                            <p className='Info--param'><b>Time track play:</b> {time}</p> :
                            <p className='Info--param'><b>Time listen:</b> {dateTime}</p>}
                    </div>
                    {publicTrack ? <p className='Info--param public-track'>опубликовано </p> :
                        <>
                            <p className='Info--param unpublic-track'>НЕ опубликовано </p>
                            <Button
                                variant="contained"
                                onClick={publish}
                            >Опубликовать</Button>
                        </>
                    }
                    {enabledBtn ? <>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={deleteTrack}
                        >Delete</Button>
                    </> : null
                    }
                </div> :
                null}
        </>
    );
};

export default Tracks;