import React, { Fragment } from 'react';
import { Container } from '@material-ui/core';
import Header from '../Header/Header';
import "./Layout.css"

const Layout = (props) =>
(
    <Fragment>
        <Container>
            <Header />
            <main className="Layout-Content">
                {props.children}
            </main>
        </Container>
    </Fragment>
);

export default Layout;