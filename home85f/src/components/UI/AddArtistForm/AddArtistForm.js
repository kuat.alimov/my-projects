import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Grid, Button } from '@material-ui/core';
import FileInput from '../Form/FileInput';
import FormElement from '../Form/FormElement';
import { addNewArtists } from '../../../store/action/artistsAction';
import Spinner from '../Spinner/Spinner';

const AddArtistForm = () => {
    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user)

    const { loading } = useSelector(state => state.artists);
    const errors = useSelector(state => state.artists.error);
    const [state, setState] = useState({
        name: "",
        description: "",
        image: "",
    });

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {
                ...prevState,
                [name]: file,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        await dispatch(addNewArtists(dataCopy, { ...user }))
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        })
        onFormSubmit(formData);
    }

    const getFieldError = (fieldName) => {
        try {
            // debugger
            return errors.errors[fieldName].message;
        } catch {
            return undefined;
        }
    }

    return (
        <>
            {loading && <Spinner />}
            <form autoComplete="off" noValidate onSubmit={submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <FormElement
                        type="text"
                        label="Name"
                        id="name"
                        value={state.name}
                        onChange={inputChangeHandler}
                        name="name"
                        error={getFieldError("name")}
                        required={true} />
                    <FormElement
                        type="text"
                        label="Description"
                        id="description"
                        value={state.description}
                        onChange={inputChangeHandler}
                        name="description"
                        required={false}
                        multiline={true}
                        rows={3} />
                    <Grid item>
                        <FileInput
                            name="image"
                            label="Image"
                            onChange={fileChangeHandler}
                        />
                    </Grid>
                    <Grid item>
                        <Button id="saveArtist"
                            variant="contained" type="submit" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default AddArtistForm;