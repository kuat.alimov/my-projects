import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Grid, Button } from '@material-ui/core';
import FileInput from '../Form/FileInput';
import FormElement from '../Form/FormElement';
import Spinner from '../Spinner/Spinner';
import { addNewAlbums } from '../../../store/action/albumAction';


const AddAlbumsForm = () => {
    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user)

    const { artistsList } = useSelector(state => state.artists);

    const { loading } = useSelector(state => state.albums);
    const errors = useSelector(state => state.albums.error);
    const [state, setState] = useState({
        album: "",
        image: "",
        artist: "",
    });

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {
                ...prevState,
                [name]: file,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        await dispatch(addNewAlbums(dataCopy, { ...user }))
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        })
        onFormSubmit(formData);
    }

    const getFieldError = (fieldName) => {
        try {
            // debugger
            return errors.errors[fieldName].message;
        } catch {
            return undefined;
        }
    }

    return (
        <>
            {loading && <Spinner />}
            <form autoComplete="off" noValidate onSubmit={submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <FormElement
                        type="text"
                        label="Album"
                        id="album"
                        value={state.album}
                        onChange={inputChangeHandler}
                        name="album"
                        error={getFieldError("album")}
                        required={true} />
                    <FormElement
                        label="Artist"
                        id="artist"
                        value={state.artist}
                        onChange={inputChangeHandler}
                        name="artist"
                        select
                        options={artistsList}
                        error={getFieldError("artist")}
                    />
                    <Grid item>
                        <FileInput
                            name="image"
                            label="Image"
                            onChange={fileChangeHandler}
                        />
                    </Grid>
                    <Grid item>
                        <Button id="saveAlbum" name="saveAlbum"
                            variant="contained" type="submit" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default AddAlbumsForm;