import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Button, makeStyles } from '@material-ui/core';
import FormElement from '../Form/FormElement';
import MaskedInput from "react-text-mask";
import { getResponseAlbums } from '../../../store/action/albumAction';
import { addNewTracks } from '../../../store/action/trackAction';
import Spinner from '../Spinner/Spinner';

const useStyles = makeStyles(theme => (
    {
        btnSave: {
            marginTop: '20px'
        },
    }
));

function TextMaskCustom(props) {
    const { inputRef, ...other } = props;

    return (
        <MaskedInput
            {...other}
            ref={(ref) => {
                inputRef(ref ? ref.inputElement : null);
            }}
            mask={[/\d/, /\d/, ":", /\d/, /\d/]}
            placeholderChar={'\u2000'}
            showMask
        />
    );
}


const AddTrackForm = () => {
    const classes = useStyles();

    const dispatch = useDispatch();

    const user = useSelector(state => state.users.user)

    const { albumsList } = useSelector(state => state.albums);

    const { loading } = useSelector(state => state.tracks);
    const errors = useSelector(state => state.tracks.error);
    const [state, setState] = useState({
        track: "",
        trackNumber: "",
        time: "0 :  ",
        album: "",
    });

    useEffect(() => {
        dispatch(getResponseAlbums(null, { ...user }));
    }, [dispatch]);

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setState(prevState => {
            return {
                ...prevState,
                [name]: value,
            }
        }
        )
    }

    const onFormSubmit = async (dataCopy) => {
        await dispatch(addNewTracks(dataCopy, { ...user }))
    }

    const submitFormHandler = (event) => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        })
        onFormSubmit(formData);
    }

    const getFieldError = (fieldName) => {
        try {
            if (errors) {
                if (errors.errors[fieldName].message) {
                    return errors.errors[fieldName].message;
                }
                if (errors.errors.price.stringValue) {
                    return 'Номер трека не коректный';
                }
            }
        } catch {
            return undefined;
        }
    }

    return (
        <>
            {loading && <Spinner />}
            <form autoComplete="off" noValidate onSubmit={submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <FormElement
                        type="text"
                        label="Track"
                        id="track"
                        value={state.track}
                        onChange={inputChangeHandler}
                        name="track"
                        error={getFieldError("track")}
                        required={true} />
                    <FormElement
                        type="text"
                        label="Track Number"
                        id="trackNumber"
                        value={state.trackNumber}
                        onChange={inputChangeHandler}
                        name="trackNumber"
                        error={getFieldError("trackNumber")}
                        required={true} />
                    <FormElement
                        type="text"
                        label="Time"
                        id="time"
                        value={state.time}
                        onChange={inputChangeHandler}
                        name="time"
                        error={getFieldError("time")}
                        InputProps={{ inputComponent: TextMaskCustom }}
                        required={true} />
                    <FormElement
                        label="Album"
                        id="album"
                        value={state.album}
                        onChange={inputChangeHandler}
                        name="album"
                        select
                        options={albumsList}
                        error={getFieldError("album")}
                        required={true}
                    />
                    <Grid item>
                        <Button id="saveTrack" name="saveTrack" className={classes.btnSave}
                            variant="contained" type="submit" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </>
    );
};

export default AddTrackForm;