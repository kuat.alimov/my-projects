const express = require('express');

const router = express.Router();

const auth = require('./middleware/auth');
const permit = require('./middleware/permit');

const Track = require('./models/Track');
const User = require('./models/User');
const TrackHistory = require('./models/TrackHistory');

const createRouter = () => {

    router.get('/', [auth, permit('admin', 'user')], async (req, res) => {

        try {

            let tracksHistoryCopy;
            let tracksHistory;
            const result = await TrackHistory.find().sort('-datetime');

            if (result.length > 0) {
                tracksHistoryCopy = await Promise.all(
                    result.map(async (element, i) => {
                        const resultTrack = await Track.findById(element.track);
                        const resultUser = await User.findById(element.user);

                        const resultCopy = {
                            datetime: result[i].datetime,
                            username: resultUser.username,
                            token: resultUser.token,
                            track: resultTrack.track,
                        }
                        return resultCopy
                    }
                    )
                )

                tracksHistory = tracksHistoryCopy.filter(trUser => trUser.token === req.user.token)
            }

            res.send(tracksHistory);
        } catch (e) {
            res.status(400).send(e)
        }

    });

    router.post('/', [auth, permit('admin', 'user')], async (req, res) => {
        const trackHistory = new TrackHistory();

        if (!req.body.track) {
            return res.status(400).send({ error: 'Error, track ID not found or wrong!' });
        }
        const track = await Track.find({ _id: req.body.track });
        if (track.length === 0) {
            return res.status(404).send({ error: 'Error, track not found!' });
        }

        trackHistory.user = req.user._id;
        trackHistory.track = track[0]._id;
        trackHistory.datetime = new Date().toISOString();
        await trackHistory.save();
        res.send(trackHistory);
    });
    return router;
}


module.exports = createRouter;