const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, '../public/uploads'),
    uploadPathMusic: path.join(rootPath, '../public/music'),
    db: {
        name: (process.env.NODE_ENV !== 'test' ? 'lastFM' : 'lastFM_test'),
        url: "mongodb://localhost"
    },
    facebook: {
        appId: "416283772962264",
        secret: "43c7b2f7007fe90f11eb93041342a2bd",
    }
}