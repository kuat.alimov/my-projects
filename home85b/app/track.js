const express = require('express');

const router = express.Router();
const multer = require('multer');
const path = require('path');
const config = require('./config');

const auth = require('./middleware/auth');
const permit = require('./middleware/permit');

const Track = require('./models/Track');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPathMusic);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});
const upload = multer({ storage });

const createRouter = () => {

    router.get('/', auth, async (req, res) => {
        let queryFilter = {};
        if (req.query.album) {
            queryFilter.album = req.query.album;
        }
        try {
            const result = await Track.find(queryFilter).populate('album').sort('trackNumber');

            res.send(result);

        } catch (e) {
            res.status(400).send(e)
        }

    });

    router.post('/', [auth, permit('admin', 'user'), upload.single('image')], async (req, res) => {
        const track = new Track(req.body);
        if (req.file) {
            track.track = req.file.filename;
        }

        try {
            await track.save();
            res.send(track);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {

        const track = await Track.findById(req.params.id);

        track.public = req.body.publicTrack;
        try {
            await track.save();
            res.send(track);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', [auth, permit('admin')], async (req, res) => {
        try {
            await Track.findByIdAndDelete({ _id: req.params.id });

            const trackDelete = { message: 'Track delete!' }
            res.send(trackDelete);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    return router;
}


module.exports = createRouter;