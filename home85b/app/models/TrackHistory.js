const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const TrackHistory = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        track: {
            type: Schema.Types.ObjectId,
            ref: 'Track',
            required: true,
        },
        datetime: {
            type: Date,
            required: true,
        },
    },
    {
        versionKey: false
    }
);

TrackHistory.plugin(idvalidator, {
    message: 'Bad ID value for {PATH}'
});

const Track = mongoose.model('TrackHistory', TrackHistory);

module.exports = Track;