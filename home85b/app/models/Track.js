const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const TrackSchema = new Schema(
    {
        track: {
            type: String,
            required: [true, props => {
                return `Поле ${props.path} должно быть заполнено`
            }]
        },
        trackNumber: {
            type: Number,
            required: [true, props => {
                return `Поле ${props.path} должно быть заполнено`
            }],
            validate: {
                validator: async value => {
                    if (!(parseInt(value) > 0)) return false;
                },
                message: `Трек не может быть нулевым или меньше нуля`,
            },
        },
        time: {
            type: String,
            required: [true, props => {
                return `Поле ${props.path} должно быть заполнено`
            }],
        },
        album: {
            type: Schema.Types.ObjectId,
            ref: 'Album',
            required: true,
        },
        public: {
            type: Boolean,
            default: false,
        }
    },
    {
        versionKey: false
    }
);

TrackSchema.plugin(idvalidator, {
    message: 'Bad ID value for {PATH}'
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;