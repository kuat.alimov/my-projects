
const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema(
    {
        album: {
            type: String,
            required: true
        },
        datetime: {
            type: Date,
            required: true,
            default: Date.now,
        },
        image: String,
        artist: {
            type: Schema.Types.ObjectId,
            ref: 'Artist',
            required: true,
        },
        public: {
            type: Boolean,
            default: false,
        }
    },
    {
        versionKey: false
    }
);
AlbumSchema.plugin(idvalidator, {
    message: 'Bad ID value for {PATH}'
})
const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;