const express = require('express');
const router = express.Router();

const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('./config');

const auth = require('./middleware/auth');
const permit = require('./middleware/permit');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({ storage });

const createRouter = () => {

    router.get('/', async (req, res) => {
        try {
            const result = await Artist.find();

            res.send(result);

        } catch (e) {
            res.status(400).send(e)
        }

    });

    router.post('/', [auth, permit('admin', 'user'), upload.single('image')], async (req, res) => {

        const artist = new Artist(req.body);
        if (req.file) {
            artist.image = req.file.filename;
        } else {
            artist.image = null;
        }

        try {
            await artist.save();
            res.send(artist);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {

        const artist = await Artist.findById(req.params.id);

        artist.public = req.body.publicArtist;
        try {
            await artist.save();
            res.send(artist);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.delete('/:id', async (req, res) => {
        try {
            const artistResult = await Artist.findById(req.params.id);

            const albumResult = await Album.find({ artist: artistResult._id });

            if (albumResult.length > 0) {
                const resultTracks = await Track.find({ album: albumResult[0]._id });

                if (resultTracks.length > 0) {
                    await Promise.all(
                        resultTracks.map(async (element) => {
                            await Track.findByIdAndDelete({ _id: element._id });
                        })
                    )
                }
                if (albumResult.length > 1) {
                    await Promise.all(
                        albumResult.map(async (element) => {
                            await Album.findByIdAndDelete({ _id: element._id });
                        })
                    )
                }
                await Album.findOneAndDelete({ artist: artistResult._id });
            }

            await Artist.findByIdAndDelete({ _id: req.params.id });

            const albumDelete = { message: 'Artist delete!' }
            res.send(albumDelete);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
}


module.exports = createRouter;