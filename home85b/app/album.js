const express = require('express');
const router = express.Router();

const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('./config');

const auth = require('./middleware/auth');
const permit = require('./middleware/permit');

const Album = require('./models/Album');
const Track = require('./models/Track');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({ storage });

const createRouter = () => {

    router.get('/', auth, async (req, res) => {
        let queryFilter = {};
        if (req.query.artist) {
            queryFilter.artist = req.query.artist;
        }
        try {
            const result = await Album.find(queryFilter).populate('artist');
            let resultCopy;

            if (result.length > 0) {
                resultCopy = await Promise.all(result.map(async (element) => {
                    const response = await Track.find({ album: element._id }).populate('album');
                    const countTrack = {
                        ...element._doc,
                        count: response.length,
                    };
                    return countTrack;
                }))
            }

            res.send(resultCopy);
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.get('/:id', auth, async (req, res) => {

        try {
            const result = await Album.findById(req.params.id).populate('artist');

            res.send(result);

        } catch (e) {
            res.status(404).send(e);
        }
    });

    router.post('/', [auth, permit('admin', 'user'), upload.single('image')], async (req, res) => {

        const album = new Album(req.body);
        if (req.file) {
            album.image = req.file.filename;
        }
        console.log(album)
        try {
            await album.save();
            res.send(album);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {

        const album = await Album.findById(req.params.id);

        album.public = req.body.publicAlbum;
        try {
            await album.save();
            res.send(album);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    router.delete('/:id', [auth, permit('admin')], async (req, res) => {
        try {
            const albumResult = await Album.findById(req.params.id);

            const resultTracks = await Track.find({ album: albumResult._id });

            if (resultTracks.length > 0) {
                await Promise.all(
                    resultTracks.map(async (element, i) => {
                        await Track.findByIdAndDelete({ _id: element._id });
                    }
                    )
                )
            }

            await Album.findByIdAndDelete({ _id: req.params.id });
            const albumDelete = { message: 'Album delete!' }
            res.send(albumDelete);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
}


module.exports = createRouter;