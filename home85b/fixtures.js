const config = require('./app/config');
const mongoose = require('mongoose');
const Album = require('./app/models/Album');
const Track = require('./app/models/Track');
const Artist = require('./app/models/Artist');
const User = require('./app/models/User');
const TrackHistory = require('./app/models/TrackHistory');

const { nanoid } = require("nanoid");

mongoose.connect(config.db.url + '/' + config.db.name)
const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('artists');
        await db.dropCollection('albums');
        await db.dropCollection('tracks');
        await db.dropCollection('users');
        await db.dropCollection('trackhistories');
    } catch (e) {
        console.log('Collections not found');
    }

    const [artistFirst, artistSecond] = await Artist.create({
        name: 'Jhon Doe',
        description: 'group vocalist Enigma',
        image: "NzSV7To7WAajg_GAu17PY.jpg",
        public: true,
    }, {
        name: 'Monroe Doerty',
        description: 'group vocalist Dogma',
        image: "Z1B1bK4FateCullXFSzw8.jpg",
        public: true,
    });

    const [albumRosess, albumTulips] = await Album.create({
        album: "Rosess",
        datetime: '2020-12-05T15:40:36.368Z',
        artist: artistFirst._id,
        image: "RKJyZ1Lj6LwtIo6FUnYc9.jpg",
        public: true,
    }, {
        album: "Tulips",
        datetime: '2020-12-06T10:10:10.100Z',
        artist: artistSecond._id,
        image: "fKwU-eZpY8VYEdyqW6ZEx.jpg",
        public: true,
    })

    const [trackRosess, trackRosessRMX, trackTulips, trackTulipsRMX] = await Track.create(
        {
            time: '2:54',
            album: albumRosess._id,
            trackNumber: 1,
            track: "Rosess.mp3",
            public: true,
        }, {
        time: '3:08',
        album: albumRosess._id,
        trackNumber: 2,
        track: "Rosess_remix.mp3",
        public: true,
    }, {
        time: '2:51',
        album: albumRosess._id,
        trackNumber: 3,
        track: "Apples.mp3",
        public: true,
    }, {
        time: '3:12',
        album: albumRosess._id,
        trackNumber: 4,
        track: "Water.mp3",
        public: true,
    }, {
        time: '2:08',
        album: albumRosess._id,
        trackNumber: 5,
        track: "Water_remix.mp3",
        public: true,
    }, {
        time: '2:47',
        album: albumTulips._id,
        trackNumber: 1,
        track: "Tulips.mp3",
        public: true,
    }, {
        time: '3:11',
        album: albumTulips._id,
        trackNumber: 2,
        track: "Tulips_remix.mp3",
        public: true,
    }, {
        time: '2:34',
        album: albumTulips._id,
        trackNumber: 3,
        track: "Pear.mp3",
        public: true,
    }, {
        time: '3:11',
        album: albumTulips._id,
        trackNumber: 4,
        track: "Vegas.mp3",
        public: true,
    }, {
        time: '2:59',
        album: albumTulips._id,
        trackNumber: 5,
        track: "Vegas_remix.mp3",
        public: true,
    },
    )

    const [admin, user] = await User.create(
        {
            username: 'admin',
            displayName: 'admin',
            email: "admin_FM@rambler.ru",
            password: "123456",
            role: 'admin',
            avatarImage: 'Z1B1bK4FateCullXFSzw8.jpg',
            token: nanoid(),
        },
        {
            username: 'user',
            displayName: 'Jon Doe',
            email: "user_FM@rambler.ru",
            password: "123456",
            avatarImage: 'NzSV7To7WAajg_GAu17PY.jpg',
            role: 'user',
            token: nanoid(),
        }
    )

    await TrackHistory.create(
        {
            user: admin._id,
            track: trackRosessRMX._id,
            datetime: '2021-01-06T15:40:36.368Z',
        },
        {
            user: user._id,
            track: trackTulips._id,
            datetime: '2021-01-07T10:10:10.100Z',
        },
        {
            user: admin._id,
            track: trackRosess._id,
            datetime: '2021-01-06T15:20:26.368Z',
        },
        {
            user: user._id,
            track: trackTulipsRMX._id,
            datetime: '2021-01-07T10:11:11.111Z',
        }
    )

    await db.close();
});
