const express = require('express');
const cors = require('cors')
const app = express();

const port = process.env.NODE_ENV !== "test" ? 8000 : 8010;
const portFront = process.env.NODE_ENV !== "test" ? 3000 : 3010;

const config = require('./app/config');

const mongoose = require('mongoose');
const artist = require('./app/artist');
const album = require('./app/album');
const track = require('./app/track');
const user = require('./app/user');
const trackHistory = require('./app/trackHistory');


const corsOptions = {
    origin: 'http://localhost:' + portFront,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));
app.use(express.json())
app.use(express.static('public'));

const run = async () => {

    await mongoose.connect(config.db.url + '/' + config.db.name, { useNewUrlParser: true })
    app.use('/artists', artist());
    app.use('/albums', album());
    app.use('/tracks', track());
    app.use('/users', user());
    app.use('/track_history', trackHistory());

    app.listen(port, () => {
        console.log(`Server started on port: ${port}`);
    });
};


run().catch(err => {
    console.error(err);
})