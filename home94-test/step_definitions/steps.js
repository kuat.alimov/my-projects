const { I } = inject();
// Add in your custom step files

Given('I have a defined step', () => {
  // TODO: replace with your own step
});

// register
Given(/^я нахожусь на странице "(.*?)"$/, () => {
  // From "features/register.feature" {"line":5,"column":1}
  I.amOnPage("/register");
  I.waitForElement("form", 10)
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  // From "features/register.feature" {"line":6,"column":1}
  I.fillField({ id: fieldName }, value);
});

When(/^я ввожу "(.*?)" в поле "(.*?)"$/, (value, fieldName) => {
  // From "features/register.feature" {"line":9,"column":1}
  I.fillField({ id: fieldName }, value);
});

When(/^загружаю фаил "(.*?)"$/, (pathToFile, fieldName) => {
  // From "features/register.feature" {"line":10,"column":1}
  I.attachFile('input[type=file]', pathToFile);
});

When('я нажимаю кнопку {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

Then('я вижу текст {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.see(text);
});
// login
When(/^теперь я на странице "(.*?)"$/, () => {
  // From "features/register.feature" {"line":5,"column":1}
  I.wait(1);
  I.amOnPage("/login");
  I.waitForElement("form", 3)
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  // From "features/register.feature" {"line":6,"column":1}
  I.fillField({ id: fieldName }, value);
});

When(/^я ввожу "(.*?)" в поле "(.*?)"$/, (value, fieldName) => {
  // From "features/register.feature" {"line":9,"column":1}
  I.fillField({ id: fieldName }, value);
});

When('я нажимаю кнопку {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

Then('я на главной вижу текст {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.see(text);
  I.wait(1);
});
//add artist
When(/^я теперь на странице артиста"(.*?)"$/, () => {
  // From "features/register.feature" {"line":5,"column":1}
  I.amOnPage("/");
});

When('я нажимаю кнопку создания артиста {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

When('я вижу в адресе {string}', (url) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  // From "features/register.feature" {"line":6,"column":1}
  I.fillField({ id: fieldName }, value);
});

When(/^я загружаю фото "(.*?)"$/, (pathToFile, fieldName) => {
  // From "features/register.feature" {"line":10,"column":1}
  I.attachFile('input[type=file]', pathToFile);
});

When('я нажимаю кнопку {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

Then('я вижу текст на главной {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.see(text);
  I.wait(2);
});
//add album
When('я теперь проверяю адрес {string}', (url) => {
  // From "features/register.feature" {"line":5,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я нажимаю кнопку создания альбома {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

When('я вижу в адресе {string}', (url) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  // From "features/register.feature" {"line":6,"column":1}
  I.fillField({ id: fieldName }, value);
});

When('я нажимаю селект {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

When('я выбираю {string} элемент {string}', (elemLI, text) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  const element = locate(elemLI).withAttr({ name: text });
  I.click(element);
});

When(/^я загружаю фото "(.*?)"$/, (pathToFile, fieldName) => {
  // From "features/register.feature" {"line":10,"column":1}
  I.attachFile('input[type=file]', pathToFile);
});

When('я нажимаю кнопку {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

Then('я на странице вижу текст {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.wait(2);
  I.see(text);
});
//track history
When('я проверяю адрес {string}', (url) => {
  // From "features/register.feature" {"line":5,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я жму элемент {string} артиста {string}', (elementDiv, text) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  const element = locate(elementDiv).withAttr({ name: text });
  I.click(element);
});

When('я жму элемент {string} альбома {string}', (elementDiv, text) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  const element = locate(elementDiv).withAttr({ name: text });
  I.click(element);
});

When('я жму элемент {string} трека {string}', (elementDiv, text) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  const element = locate(elementDiv).withAttr({ name: text });
  I.click(element);
});

When(/^я перехожу на страницу "(.*?)"$/, () => {
  // From "features/register.feature" {"line":5,"column":1}
  I.wait(1);
  I.amOnPage("/track_history");
});

Then('я вижу на этой странице текст {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.see(text);
  I.wait(2);
  I.amOnPage("/");
});

When(/^теперь я на главной"(.*?)"$/, () => {
  // From "features/register.feature" {"line":5,"column":1}
  I.amOnPage("/");
});
//add track
When('я теперь проверяю адрес страницы {string}', (url) => {
  // From "features/register.feature" {"line":5,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я нажимаю кнопку создания трека {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.click({ id: button });
});

When('я в адресе вижу {string}', (url) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.seeCurrentUrlEquals(url);
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  // From "features/register.feature" {"line":6,"column":1}
  I.fillField({ id: fieldName }, value);
});

When('я выбираю селект альбом {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  I.click({ id: button });
});

When('я выбираю элемент {string} альбом {string}', (elemLI, text) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(1);
  const element = locate(elemLI).withAttr({ name: text });
  I.click(element);
});

When('я жму кнопку {string}', (button) => {
  // From "features/register.feature" {"line":11,"column":1}
  I.wait(2);
  I.click({ name: button });
});

Then('я вижу на странице текст {string}', (text) => {
  // From "features/register.feature" {"line":12,"column":1}
  I.see(text);
});